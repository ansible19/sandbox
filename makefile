SHELL := /bin/bash

NC := "\e[0m"
BLUE := "\e[1;34m"


INFO := @bash -c 'printf $(BLUE); echo "====> $$0"; printf $(NC)'

DOCKER := @docker build --build-arg role=${role} --build-arg action=${action} -t test_role -f sandbox/dockerfile . && \
	    docker run -v /var/run/docker.sock:/var/run/docker.sock test_role

test:
	${INFO} "${action} ${role} role"
	${DOCKER}


clean_docker:
	${INFO} "Clean Unused Docker images and containers"
	@docker system prune -f --volumes
