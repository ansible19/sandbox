# Ansible Galaxy roles

This project aims to create all needed **Ansible** Roles and push them into **Ansible Galaxy** to be able to use them by the community.

## Folder Structure

```shell
|--root
|  |--roles     #folder Contains all ansible roles
|  |--sandbox
|  |  |-- dockerfile
|  |--makefile
|  |-- .gitignore
|  |-- .gitmodules
```

### roles

This folder contains all **Ansible** roles that will be pushed to **Ansible-galaxy**

### Sandbox

This folder contains the test environment to validate the roles.
